---
name: "Xiaomi Poco X3 NFC"
comment: "community device"
deviceType: "phone"
description: "With it's smooth 120Hz display, affordable price and long-lasting battery, the Xiaomi Poco X3 NFC stands as a great option for those users who need a safe, decent Linux phone option for their daily tasks."
image: "https://raw.githubusercontent.com/ubuntu-touch-surya/ubuntu-touch-surya/master/ubports.png"
price:
  avg: 200

deviceInfo:
  - id: "cpu"
    value: "Octa-core (2x2.3 GHz Kryo 470 Gold & 6x1.8 GHz Kryo 470 Silver)"
  - id: "chipset"
    value: "Qualcomm SM7150-AC Snapdragon 732G (8 nm)"
  - id: "gpu"
    value: "Adreno 618"
  - id: "rom"
    value: "64GB/128GB"
  - id: "ram"
    value: "6GB"
  - id: "android"
    value: "Android 10"
  - id: "battery"
    value: "5160 mAh"
  - id: "display"
    value: "IPS LCD, 120Hz, HDR10, 450 nits (typ), 6.67 inches, 107.4 cm2 (~84.6% screen-to-body ratio), 1080 x 2400 pixels, 20:9 ratio (~395 ppi density)"
  - id: "arch"
    value: "arm64"
  - id: "rearCamera"
    value: 64 MP, f/1.9, (wide), 1/1.73", 0.8µm, PDAF, 13 MP, f/2.2, 119˚ (ultrawide), 1.0µm, 2 MP, f/2.4, (macro), 2 MP, f/2.4, (depth)
  - id: "frontCamera"
    value: 20 MP, f/2.2, (wide), 1/3.4", 0.8µm
  - id: "dimensions"
    value: "165.3 x 76.8 x 9.4 mm (6.51 x 3.02 x 0.37 in)"
  - id: "weight"
    value: "215 g (7.58 oz)"
contributors:
  - name: "ungeskriptet"
    forum: "https://forums.ubports.com/user/ungeskriptet"
    photo: "https://forums.ubports.com/assets/uploads/profile/3163-profileavatar-1645281074839.png"
  - name: "ywmaa"
    forum: "https://github.com/ywmaa"
    photo: ""
communityHelp:
  - name: "Telegram - @ut_pocox3"
    link: "https://t.me/ut_pocox3"
externalLinks:
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-poco-x3/xiaomi-surya/-/issues"
  - name: "Device Source"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-poco-x3/xiaomi-surya"
  - name: "Kernel source"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-poco-x3/kernel-xiaomi-surya"
---
