---
name: "Xiaomi Redmi 5 Plus"
comment: "wip"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 2.02 GHz ARM Cortex A53"
  - id: "chipset"
    value: "Qualcomm Snapdragon 625, MSM8953"
  - id: "gpu"
    value: "Adreno 506"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3/4GB"
  - id: "android"
    value: "7.1.2 (Nougat)"
  - id: "battery"
    value: "4000 mAh"
  - id: "display"
    value: '1080x2160 pixels, 5.99"'
  - id: "rearCamera"
    value: "12MP, PDAF"
  - id: "frontCamera"
    value: "5 MP"

contributors:
  - name: "BirdZhang"
    forum: ""
    photo: ""
    role: "maintainer"
  - name: "Bartek"
    forum: ""
    photo: ""
    role: "maintainer"
  - name: "Asdew"
    forum: ""
    photo: ""
    role: "contributor"

externalLinks:
  - name: "Source repos"
    link: "https://github.com/ubports-on-vince"
  - name: "CI builds"
    link: "https://github.com/ubports-on-vince/ubports-ci"

seo:
  description: "Flash your Xiaomi Redmi 5 Plus smartphone with the latest version of the Ubuntu Touch operating system, a privacy focused OS developed by hundreds of people."
  keywords: "Ubuntu Touch, Xiaomi Redmi 5, Privacy Focus OS, Linux on Phone"
---
