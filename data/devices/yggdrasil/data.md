---
name: "Volla Phone"
deviceType: "phone"
buyLink: "https://volla.online/de/shop/"
description: "Volla Phone, A startup focused on simplicity and privacy for the average user. It’s a solid mid-range device with eight-core, produced by Gigaset. This device falls nicely in the hands, and it has an outstanding screen/body ratio. On the other hand, it has slight incompatibility with the UT navigation bar. But, overall, it is working very smoothly and relatively responsive."
tag: "promoted"
subforum: "90/vollaphone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53 (4x 2.0 GHz + 4x 1.5 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P23, MT6763V"
  - id: "gpu"
    value: "ARM Mali-G71 MP2 @ 770 MHz, 2 cores"
  - id: "rom"
    value: "64 GB, eMMC"
  - id: "ram"
    value: "4 GB, DDR3"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "4700 mAh, 18.1 Wh, Li-Polymer"
  - id: "display"
    value: '6.3" IPS, 1080 x 2340 (409 PPI), V-notch, Rounded corners'
  - id: "rearCamera"
    value: "16MP (f/2.0, 1080p30 video) + 2MP (for bokeh/depth), PDAF, LED flash"
  - id: "frontCamera"
    value: "16MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "157 mm x 75.1 mm x 9.65 mm"
  - id: "weight"
    value: "190 g"
contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
    photo: ""
  - name: Deathmist
    forum: https://forums.ubports.com/user/deathmist
    photo: https://forums.ubports.com/assets/uploads/profile/3171-profileavatar-1613145487767.png
externalLinks:
  - name: "Telegram Volla Group"
    link: "https://t.me/utonvolla"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/90/vollaphone"
  - name: "Report a bug"
    link: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues"
  - name: "Device source"
    link: "https://github.com/HelloVolla/android_device_volla_yggdrasil/tree/halium-9.0"
  - name: "Kernel source"
    link: "https://github.com/HelloVolla/android_kernel_volla_mt6763/tree/halium-9.0"
  - name: "Halium build manifest"
    link: "https://github.com/Halium/halium-devices/blob/halium-9.0/manifests/volla_yggdrasil.xml"

seo:
  description: "Get your Volla Phone with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhone, Volla Phone, Linux Phone"
---
