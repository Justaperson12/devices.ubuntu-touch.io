---
name: "Volla Phone 22"
deviceType: "phone"
image: "https://volla.online/de/resources/Store/Volla-Phone-22/Volla-22_Black.png"
buyLink: "https://volla.online/de/shop/volla-phone-22/"
description: "The Volla Phone 22 sets a new standard for convenience, design and performance. It combines the unique ease of use with uncompromising respect of privacy through an independent, open source operating system. The device has a 48 megapixel camera with wide-angle option, 128 GB internal memory, 8-core processor with MediaTek HyperEngine technology and a removable battery. You can get the device pre-installed with Ubuntu Touch or as a 2nd operating system with the unique multi-boot feature of Volla OS."
tag: "promoted"
subforum: "112/volla-phone-22"
price:
  avg: 452
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM (2x Cortex-A75 @ 2.0 GHz + 6x Cortex-A55 @ 1.8 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio G85, MT6769Z"
  - id: "gpu"
    value: "ARM Mali-G52 MC2 @ 1000 MHz, 2 cores"
  - id: "rom"
    value: "128 GB, eMMC"
  - id: "ram"
    value: "4 GB, LPDDR4X @ 1800 MHz"
  - id: "android"
    value: "11.0"
  - id: "battery"
    value: "4500 mAh, Li-Polymer"
  - id: "display"
    value: '6.3" IPS, 1080 x 2340 (410 PPI), V-notch, Rounded corners'
  - id: "rearCamera"
    value: "48MP (f/1.8, 1440p30 video) + 8MP (ultra wide-angle / macro lens), PDAF, LED flash"
  - id: "frontCamera"
    value: "16MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "157.5 mm x 75 mm x 10.4 mm"
  - id: "weight"
    value: "210 g"
  - id: "releaseDate"
    value: "July 2022"

contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
  - name: "TheKit"
    forum: "https://forums.ubports.com/user/thekit"
    photo: ""
  - name: "neochapay"
    forum: ""
    photo: ""
  - name: "Deathmist"
    forum: "https://forums.ubports.com/user/deathmist"
    photo: "https://forums.ubports.com/assets/uploads/profile/3171-profileavatar-1613145487767.png"

communityHelp:
  - name: "Telegram - @utonvolla"
    link: "https://t.me/utonvolla"

externalLinks:
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/112/volla-phone-22"
  - name: "Report a bug"
    link: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues"
  - name: "Device source"
    link: "https://gitlab.com/ubports/porting/community-ports/android11/volla-phone-22/volla-mimameid"
  - name: "Kernel source"
    link: "https://gitlab.com/ubports/porting/community-ports/android11/volla-phone-22/kernel-volla-mt6768"

seo:
  description: "Get your Volla Phone 22 with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhone22, Volla Phone 22, Privacy Phone"
---
