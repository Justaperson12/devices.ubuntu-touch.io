---
name: "Oneplus 2"
comment: "wip"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/oneplus2.png"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 1.8 GHz Cortex A57 + quad-core 1.5 GHz Cortex A53"
  - id: "chipset"
    value: "Qualcomm MSM8994 Snapdragon 810"
  - id: "gpu"
    value: "Adreno 430"
  - id: "rom"
    value: "16/64 GB"
  - id: "ram"
    value: "3/4 GB"
  - id: "display"
    value: "5.5 in, 1920 x 1080"
  - id: "rearCamera"
    value: "13 MP, dual-LED flash"
  - id: "frontCamera"
    value: "5 MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "151.8 x 74.9 x 9.85 mm"
  - id: "releaseDate"
    value: "July 2015"

externalLinks:
  - name: "Forum Post"
    link: "https://forums.ubports.com/topic/4253/oneplus-2"
  - name: "Device Tree"
    link: "https://github.com/Halium/android_device_oneplus_oneplus2"
  - name: "Kernel repository"
    link: "https://github.com/Halium/android_kernel_oneplus_msm8994"

contributors:
  - name: "Vince1171"
    forum: ""
    photo: ""
---
