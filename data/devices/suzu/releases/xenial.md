---
portType: "Halium 7.1"
kernelVersion: "4.4.146"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "sdCard"
        value: "x"
      - id: "rtcTime"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
      - id: "waydroid"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
        overrideGlobal: true
      - id: "nfc"
        value: "-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/245"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
---

### Preparatory steps

You can install Ubuntu Touch on the versions F5121 and F5122 of the Sony Xperia X.
Before, you have to take a number of preparatory steps:

1. Ensure you have upgraded the stock firmware at least to Android 8, else you can't flash the OEM binaries for AOSP. It is best to ensure that you have all the latest firmware installed.
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) for your device,
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking,
4. Install ADB on your computer
5. **BEFORE unlocking the boot loader**, [back up your TA partition](https://together.jolla.com/question/168711/xperia-x-backup-ta-partition-before-unlocking-bootloader/), in case you later wish to return to factory state. **You can't do this step at a later stage!**
6. [Get an unlocking code from Sony](https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/),
7. Reboot to fastboot and unlock the bootloader using the code obtained from Sony
8. Download the [OEM binaries for AOSP from sony](https://developer.sony.com/file/download/software-binaries-for-aosp-nougat-android-7-1-kernel-4-4-loire/), unpack them,
9. Flash them in fastboot (`fastboot flash oem [filename]`)
