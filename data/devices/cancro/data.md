---
name: "Xiaomi 4"
deviceType: "phone"

externalLinks:
  - name: "Repository"
    link: "https://gitlab.com/ubports/community-ports/cancro"

seo:
  description: "Switch your Xiaomi 4 OS to Ubuntu Touch, as your daily driver, a privacy focused OS."
  keywords: "Ubuntu Touch, Xiaomi 4, linux for smartphone, Linux on Phone"
---
