---
name: "Sony Xperia Z4 Tablet (LTE or Wi-fi only) "
deviceType: "tablet"

deviceInfo:
  - id: "cpu"
    value: "Octa-core Cortex-A53 & Cortex-A574 x 2.0 Ghz + 4 x 1.5 Ghz"
  - id: "chipset"
    value: "Qualcomm MSM8994 Snapdragon 810"
  - id: "gpu"
    value: "Qualcomm Adreno 430"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 7.1 Nougat"
  - id: "battery"
    value: "Non-removable Li-Ion 6000 mAh"
  - id: "display"
    value: "10,1 in, IPS, 2560x1600 (299 PPI)"
  - id: "rearCamera"
    value: "8.1 MP F/2.0 LED flash"
  - id: "frontCamera"
    value: "5.1 MP F/2.4 no flash, 30 fps"

contributors:
  - name: "Guf"
    forum: "https://forums.ubports.com/user/guf"
    photo: ""

externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/topic/4351/porting-ubuntu-touch-ubports-to-karin-sony-xperia-z4-tablet-sgp771-and-karin_windy-sony-xperia-z4-tablet-sgp712"
  - name: "Report a bug"
    link: "https://github.com/ubports/ubuntu-touch/issues"
  - name: "Sources"
    link: "https://github.com/sonyxperiadev/device-sony-karin"
  - name: "Kernel sources"
    link: "https://github.com/fredldotme/device-kernel-loire"
  - name: "Device sources"
    link: "https://github.com/ubports/android_device_sony_karin"
---
