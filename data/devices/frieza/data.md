---
name: "BQ Aquaris M10 FHD"
deviceType: "tablet"
description: "The first Ubuntu tablet and the first to offer desktop-mobile convergence, the rear camera shoots surprisingly good stills, particularly HDR mode. A quad-core powers the M10, 64-bit MediaTek MT8163 SoC clocked at 1.3GHz in the HD model and 1.5GHz in the FHD model reviewed here. The GPU is a Mali-T720 MP2, clocked up to 520MHz (HD) and 600MHz (FHD). Both models have 2GB of RAM and 16GB of internal storage, which can be expanded by up to 64GB by plugging a card into the external MicroSD slot."
subforum: "75/bq-m10-hd"

deviceInfo:
  - id: "cpu"
    value: "Quad-Core Cortex-A53 1.5Ghz"
  - id: "chipset"
    value: "MediaTek MT8163A"
  - id: "gpu"
    value: "Mali-T720MP2"
  - id: "rom"
    value: "16GB"
  - id: "ram"
    value: "2GB"
  - id: "android"
    value: "Android 5.1"
  - id: "battery"
    value: "7280 mAh"
  - id: "display"
    value: "1920x1200 pixels, 10.1 in"
  - id: "rearCamera"
    value: "8MP"
  - id: "frontCamera"
    value: "5MP"

contributors:
  - name: "BQ"
    role: "Phone maker"
    expired: true

externalLinks:
  - name: "Telegram"
    link: "https://t.me/joinchat/ubports"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/75/bq-m10-hd"
  - name: "Report a bug"
    link: "https://github.com/ubports/ubuntu-touch/issues"
---
