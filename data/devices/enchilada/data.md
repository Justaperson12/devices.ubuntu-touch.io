---
name: "OnePlus 6"
comment: "community device"
description: "The OnePlus 6-series is the flagship series made by OnePlus in 2018 year. Flagship CPU Snapdragon 845 in combination with powerful battery provide good daily Ubuntu Touch usage experience. With 6” AMOLED screen and loud speaker you can watch any video with pleasure. What else? Of course good quality 16MP camera, Type-C USB and headphone jack"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x 2.8 GHz Kryo 385 Gold & 4x 1.7 GHz Kryo 385 Silver)"
  - id: "chipset"
    value: "Qualcomm SDM845 Snapdragon 845"
  - id: "gpu"
    value: "Adreno 630"
  - id: "rom"
    value: "64/128/256 GB UFS2.1"
  - id: "ram"
    value: "6/8 GB LPDDR4X"
  - id: "android"
    value: "OxygenOS 9 (Android 9)"
  - id: "battery"
    value: "Non-removable Li-Po 3400 mAh"
  - id: "display"
    value: "1080x2200 pixels, 6.28 in"
  - id: "rearCamera"
    value: "16 MP"
  - id: "frontCamera"
    value: "16 MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "155.70 x 75.40 x 7.75"
  - id: "weight"
    value: "177 g"

contributors:
  - name: "SevralT"
    photo: "https://avatars.githubusercontent.com/u/77262770?v=4"
    forum: "https://forums.ubports.com/user/sevralt"
  - name: "calebccff"
    photo: ""
    forum: ""
  - name: "MrCyjanek"
    photo: ""
    forum: ""

externalLinks:
  - name: "Telegram group"
    link: "https://t.me/ubports_op6"
  - name: "Kernel sources"
    link: "https://gitlab.com/ubports/community-ports/android9/oneplus-6/kernel-oneplus-sdm845"
  - name: "Device overlay"
    link: "https://gitlab.com/ubports/community-ports/android9/oneplus-6/oneplus-enchilada-fajita"
  - name: "CI Builds"
    link: "https://gitlab.com/ubports/community-ports/android9/oneplus-6/oneplus-enchilada-fajita/-/pipelines"

seo:
  description: "Switch your Oneplus 6 or 6T to Ubuntu Touch, as your open source daily driver OS."
  keywords: "Ubuntu Touch, Oneplus 6, Oneplus 6T, Linux on Mobile"
---
