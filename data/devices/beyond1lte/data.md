---
name: "Samsung Galaxy S10 (Exynos)"
deviceType: "phone"
description: "Samsung's flagship for 2019. Features a in-screen fingerprint sensor, a Exynos 9820 SOC and a holepunch display."

deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Samsung Exynos 9 Series 9820"
  - id: "gpu"
    value: "Mali-G76 MP12"
  - id: "rom"
    value: "128/512 GB"
  - id: "ram"
    value: "8 GB"
  - id: "android"
    value: "Android 9"
  - id: "battery"
    value: "3400 mAh"
  - id: "display"
    value: "1440x3040 pixels, 6.4 in"
  - id: "rearCamera"
    value: "12MP"
  - id: "frontCamera"
    value: "10MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "149.9 x 70.4 x 7.8 mm (5.90 x 2.77 x 0.31 in)"
  - id: "weight"
    value: "157g"

contributors:
  - name: Kreato
    forum: ""
    photo: ""

externalLinks:
  - name: "Telegram - @utbeyondxlte"
    link: "https://t.me/utbeyondxlte"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/android10/samsung-galaxy-s10/samsung-exynos9820/-/issues"
  - name: "Device source"
    link: "https://gitlab.com/ubports/community-ports/android10/samsung-galaxy-s10/samsung-exynos9820"
  - name: "Kernel Repository"
    link: "https://gitlab.com/ubports/community-ports/android10/samsung-galaxy-s10/kernel-samsung-exynos9820"
  - name: "CI Builds"
    link: "https://gitlab.com/ubports/community-ports/android10/samsung-galaxy-s10/samsung-exynos9820/-/pipelines"
---
