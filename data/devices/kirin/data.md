---
name: "Sony Xperia 10"
comment: "community device"
deviceType: "phone"
description: "Ubuntu Touch on the Sony Xperia 10 is a great experience as the gesture based interface is easy to use on the brilliant 21:9 radio screen."

deviceInfo:
  - id: "cpu"
    value: "Octa-core 2.2 GHz Cortex-A53"
  - id: "chipset"
    value: "Qualcomm SDM630 Snapdragon 630"
  - id: "gpu"
    value: "Adreno 508"
  - id: "rom"
    value: "64 GB"
  - id: "ram"
    value: "4 GB"
  - id: "battery"
    value: "2870 mAh"
  - id: "display"
    value: '6" 1080x2520 IPS'
  - id: "rearCamera"
    value: "13MP, f/2.0, LED Flash"
  - id: "frontCamera"
    value: "8MP, f/2.0″"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "156 x 68 x 8.4 mm"

contributors:
  - name: "HengYeDev"
    photo: ""
    forum: "https://forums.ubports.com/user/hengyedev"

externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
  - name: "Forum Thread"
    link: "https://forums.ubports.com/topic/5452/sony-xperia-10-kirin"
  - name: "Kernel Source"
    link: "https://gitlab.com/ubports/community-ports/android9/sony-xperia-10/kernel-sony-kirin"
  - name: "Device Build"
    link: "https://gitlab.com/ubports/community-ports/android9/sony-xperia-10/sony-kirin"
---
