import portStatus from "@data/portStatus.json";
import deviceInfo from "@data/deviceInfo.json";
import progressStages from "@data/progressStages.json";
import ignoredCommits from "@data/ignoredCommits.json";
import portType from "@data/portType.json";
import releaseChannels from "@data/releaseChannels.json";
import releases from "@data/releases.json";
import linkDetection from "@data/linkDetection.json";
import supportStatus from "@data/supportStatus.json";

import fetchData from "./fetchData.js";
import getGitdata from "./getGitdata.js";
import * as dataUtils from "./dataUtils.js";

import slugify from "@sindresorhus/slugify";
import { decode } from "html-entities";
import pMemoize from "p-memoize";

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

async function elaborate(memoCode, device) {
  const deviceData = await fetchData("memo-device");
  const installerData = deviceData.installerData;
  const releaseChannelData = deviceData.releaseChannelData;
  const pushServerStats = deviceData.pushServerStats;
  const currencyConversions = deviceData.currencyConversions;

  // Set codename and path from file name
  device.path = "/device/" + slugify(device.codename, { decamelize: false });
  if (device.release != releases.default) {
    device.defaultReleasePath =
      device.allReleases.findIndex((r) => r.name == releases.default) != -1
        ? device.path
        : null;
    device.path += "/release/" + device.release;
  }

  // Get file history from git
  device.gitData = [].concat(
    getGitdata(device.filePath),
    getGitdata(
      process.cwd() +
        "/data/devices/" +
        (device.variantOf || device.codename) +
        ".md"
    )
  );
  removeIgnoredCommits(device);
  detectLinkIcons(device);
  checkMaintained(device);

  // Set feature default and global values
  if (device.portStatus) {
    addMissingFeatures(device);
    addGlobalState(device);
    addConditionalStates(device);
    deleteUnavailableFeatures(device);
  }

  // Calculate progress and stages
  calculateProgress(device);
  calculateProgressStage(device);
  countUntestedFeatures(device);

  if (device.portStatus) {
    nextStageRequirements(device);

    // Clean categories
    useNameInsteadOfFeatureId(device);
    deleteVoidCategories(device);
  }

  if (device.deviceInfo) {
    useNameInsteadOfSpecificationId(device);
  }

  // Calculate average price if min and max is known
  calculateAveragePrice(device, currencyConversions);

  // Check wired display and bluetooth for convergence
  isConvergenceEnabled(device);

  // Add port type description from port type
  addPortTypeDescription(device);

  // Elaborate fetched data
  getInstallerSupport(device, installerData);
  addProgressWeight(device);
  calculateSupportStatus(device);
  if (pushServerStats) getPushStats(device, pushServerStats);
  await getReleaseChannels(device, releaseChannelData, installerData.alias);
  await downloadForumData(device);
  if (!device.noInstall)
    await getInstructionsFromInstaller(device, installerData);
  getEnabledSections(device);

  // Import and set images
  await setImportedImages(device);

  return device;
}

/* Functions */

// Get installation instructions from installer configs
async function getInstructionsFromInstaller(device, installerData) {
  try {
    const configCodename =
      installerData.alias[device.codename] || device.codename;
    const response = await fetch(
      "https://ubports.github.io/installer-configs/v2/devices/" +
        configCodename +
        ".json"
    );
    const data = await response.json();
    let osSection = data.operating_systems.find(
      (os) => os.name == "Ubuntu Touch"
    );
    device.installerInstructions = {
      unlock: data?.unlock.map((stepId) => data?.user_actions[stepId]) || [],
      prerequisites:
        osSection?.prerequisites.map((stepId) => data?.user_actions[stepId]) ||
        [],
      downloads:
        []
          .concat(
            ...osSection?.steps
              .filter((s) =>
                s?.actions.some((a) => Object.hasOwn(a, "core:manual_download"))
              )
              .map((s) =>
                s.actions.filter((a) =>
                  Object.hasOwn(a, "core:manual_download")
                )
              )
          )
          .map((a) => a["core:manual_download"].file) || []
    };
  } catch (e) {
    console.log(
      ansiCodes.redFG + "%s" + ansiCodes.reset,
      "Install instructions download failed for: " + device.codename
    );
    device.installerInstructions = {};
  }
}

// Download forum data
async function downloadForumData(device) {
  if (device.subforum) {
    try {
      const response = await fetch(
        "https://forums.ubports.com/api/category/" + device.subforum
      );
      const data = await response.json();
      device.forumTopics = data.topics
        .filter((topic) => !topic.deleted)
        .map((topic) => {
          return {
            title: decode(topic.title),
            user: decode(topic.user.username),
            timestamp: topic.timestamp,
            views: topic.viewcount,
            link: "https://forums.ubports.com/topic/" + topic.slug,
            locked: topic.locked,
            pinned: topic.pinned
          };
        });
    } catch (e) {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Forum topic data download failed for: " + device.codename
      );
      device.forumTopics = [];
    }
  } else {
    device.forumTopics = [];
  }
}

// Download release channel data
async function downloadCurrentChannelData(path) {
  try {
    const response = await fetch("https://system-image.ubports.com" + path);
    const data = await response.json();
    return data;
  } catch (e) {
    // Display a warning if release channel data wasn't downloaded
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Failed to download release channel data."
    );
    return {};
  }
}

// Add weight to installer and port data
function addProgressWeight(device) {
  device.progress = device.noInstall ? device.progress * 0.9 : device.progress;
  device.progress = device.portStatus
    ? device.progress
    : device.progress * 0.95;
  device.progress = Math.round(1000 * device.progress) / 10;
}

// Add missing features from defaults
function addMissingFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (!graphQlFeature) {
      // Don't add unavailable features
      if (feature.default != "x") {
        category.features.push({
          id: feature.id,
          name: feature.name,
          value: feature.default ? feature.default : "?"
        });
      }
    }
  });
}

// Check global state of the feature
function addGlobalState(device) {
  let featureScale = ["x", "-", "?", "+-", "+"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (
      graphQlFeature &&
      !graphQlFeature.overrideGlobal &&
      feature.global &&
      featureScale.indexOf(feature.global) <=
        featureScale.indexOf(graphQlFeature.value)
    ) {
      graphQlFeature.value = feature.global;
      graphQlFeature.bugTracker = feature.bugTracker ? feature.bugTracker : "";
      graphQlFeature.global = true;
    }
  });
}

// Mark state of the feature based on other conditions (mostly port type)
function addConditionalStates(device) {
  let featureScale = ["x", "-", "?", "+-", "+"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (feature.conditions) {
      for (let condition of feature.conditions) {
        if (
          graphQlFeature &&
          !graphQlFeature.overrideGlobal &&
          device[condition.field] == condition.value &&
          featureScale.indexOf(condition.global) <=
            featureScale.indexOf(graphQlFeature.value)
        ) {
          graphQlFeature.value = condition.global;
          graphQlFeature.bugTracker = condition.bugTracker
            ? condition.bugTracker
            : "";
          graphQlFeature.global = true;
        }
      }
    }
  });
}

// Delete unavailable features
function deleteUnavailableFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature && graphQlFeature.value == "x") {
      category.features.splice(category.features.indexOf(graphQlFeature), 1);
    }
  });
}

// Get installer compatibility data
function getInstallerSupport(device, installerData) {
  let configCodename = installerData.alias[device.codename] || device.codename;
  let deviceInstaller = installerData.config.some(
    (el) =>
      el.codename == configCodename &&
      el.operating_systems.includes("Ubuntu Touch")
  );
  device.noInstall = !deviceInstaller;
}

// Match device using codename on the push server and calculate community size score
function getPushStats(device, pushServerStats) {
  const devicePush = pushServerStats.devices.find(
    (el) => el.type == device.codename
  );
  if (devicePush) {
    let ranking = Math.ceil(Math.log(devicePush.data["5min"]) / Math.log(4));
    device.communitySize = ranking >= 1 ? ranking : 1;
  } else {
    device.communitySize = 0;
  }
}

// Get release channel data
async function getReleaseChannels(
  device,
  releaseChannelData,
  installerAliases
) {
  let allChannels = [];
  let configCodename = installerAliases[device.codename] || device.codename;
  for (let chID in releaseChannelData) {
    if (
      !releaseChannelData[chID].hidden &&
      releaseChannelData[chID].devices[configCodename]
    ) {
      let currentChannel = await downloadCurrentChannelData(
        releaseChannelData[chID].devices[configCodename].index
      );
      if (!currentChannel?.images?.length) continue;
      let lastOtaRelease = currentChannel.images[
        currentChannel.images.length - 1
      ].version_detail
        .split(",")
        .reduce((previousValue, currentValue, currentIndex, array) => {
          previousValue[currentValue.split("=").shift()] = currentValue
            .split("=")
            .pop();
          return previousValue;
        }, {});
      let channelName = chID.replace(/^(ubports\-touch\/)/, "");
      let releaseDate =
        lastOtaRelease?.ubports
          ?.slice(0, 8)
          .replace(/(\d{4})(\d{2})(\d{2})/, "$1-$2-$3") ||
        lastOtaRelease?.rootfs?.slice(0, 10) ||
        "1970-01-01";
      let ubuntuRelease = channelName.split("/").shift();
      allChannels.push({
        channel: channelName.split("/").pop(),
        fullName: chID,
        ubuntuRelease:
          ubuntuRelease == "16.04"
            ? "Xenial"
            : ubuntuRelease == "20.04"
            ? "Focal"
            : ubuntuRelease,
        version: lastOtaRelease.tag || releaseDate,
        releaseDate: new Date(releaseDate),
        description: releaseChannels[channelName.split("/").pop()].description,
        order: releaseChannels[channelName.split("/").pop()]?.order || 999
      });
    }
  }
  device.releaseChannels = allChannels.sort((a, b) => a.order - b.order);
}

// Calculate porting progress from feature matrix ( use maturity field as fallback )
function calculateProgress(device) {
  if (device.portStatus) {
    let totalWeight = 0,
      currentWeight = 0;

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        totalWeight += feature.weight;
        currentWeight += graphQlFeature.value == "+" ? feature.weight : 0;
      }
    });

    device.progress = currentWeight / totalWeight;
  } else {
    // Fallback from maturity
    device.progress = device.maturity;
  }
}

// Count number of untested features
function countUntestedFeatures(device) {
  if (device.portStatus) {
    let untestedCount = 0;

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        untestedCount += graphQlFeature.value == "?" ? 1 : 0;
      }
    });

    device.untestedCount = untestedCount;
  } else {
    device.untestedCount = 0;
  }
}

// Calculate progress stage
function calculateProgressStage(device) {
  if (device.portStatus) {
    let currentStageIndex = progressStages.length - 1; // Daily-driver ready

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature && graphQlFeature.value != "+") {
        if (currentStageIndex >= feature.stage && feature.stage > 0) {
          currentStageIndex = feature.stage - 1;
        }
      }
    });

    device.progressStage = {
      number: currentStageIndex,
      name: progressStages[currentStageIndex].name,
      description: progressStages[currentStageIndex].description
    };
  } else {
    // Fallback from maturity
    device.progressStage = {
      number: 0,
      name: progressStages[0].name,
      description: progressStages[0].description
    };
  }
}

// Calculate support status from data
function calculateSupportStatus(device) {
  let supportIndex = 0;
  if (
    device.phoneMakerSupport &&
    device.portType != "Legacy" &&
    device.progressStage.number >= 4 &&
    (!device.noInstall || device.portType == "Native") &&
    device.isMaintained
  )
    supportIndex = 3;
  else if (
    device.progressStage.number >= 4 &&
    (!device.noInstall || device.portType == "Native") &&
    device.isMaintained
  )
    supportIndex = 2;
  else if (device.isMaintained) supportIndex = 1;

  device.supportStatus = {
    number: supportIndex,
    name: supportStatus[supportIndex].name,
    description: supportStatus[supportIndex].description
  };
}

// Get next stage and required features to reach it
function nextStageRequirements(device) {
  let missing = [];
  let nextStage = device.progressStage.number + 1;

  if (nextStage == progressStages.length) return;

  dataUtils.forEachFeature(device, function (feature, category) {
    if (feature.stage == nextStage) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);
      if (graphQlFeature && graphQlFeature.value != "+") {
        missing.push(category.categoryName + ": " + feature.name);
      }
    }
  });

  device.nextProgressStage = {
    number: nextStage,
    name: progressStages[nextStage].name,
    description: progressStages[nextStage].description
  };
  device.nextStageRequirements = missing;
}

// Add wired convergence availability
function isConvergenceEnabled(device) {
  try {
    let wiredExternalMonitor = dataUtils.getFeatureById(
      dataUtils.getCategoryByName(device, "USB"),
      "wiredExternalMonitor"
    );

    let bluetooth = dataUtils.getFeatureById(
      dataUtils.getCategoryByName(device, "Network"),
      "bluetooth"
    );

    device.isConvergenceEnabled =
      (bluetooth.value == "+" || bluetooth.value == "+-") &&
      (wiredExternalMonitor.value == "+" || wiredExternalMonitor.value == "+-");
  } catch (e) {
    device.isConvergenceEnabled = false;
  }
}

// Add port type description
function addPortTypeDescription(device) {
  if (device.portType) {
    device.portTypeDescription = portType[device.portType]
      ? portType[device.portType]
      : portType["Unknown"];
  } else {
    device.portTypeDescription = portType["Unknown"];
  }
}

// Get list of enabled sections
function getEnabledSections(device) {
  device.enabledSections = {
    portStatus: !!device.portStatus,
    deviceSpec: !!device.deviceInfo,
    helpWanted:
      device.untestedCount > 0 || (!device.noInstall && device.progress > 95),
    docLinks: !!device.docLinks,
    portContributors: !!device.contributors,
    installerDownload: !device.noInstall,
    discussionsbar:
      !!device.forumTopics || !!device.communityHelp || !!device.notValid
  };
}

// Calculate average price
async function calculateAveragePrice(device, currencyConversions) {
  if (device.price) {
    if (!device.price?.avg && device.price?.min && device.price?.max)
      device.price.avg = (device.price.min + device.price.max) / 2;
    device.price.sortableAvg =
      device.price?.currency && device.price?.currency != "USD"
        ? (
            device.price?.avg / currencyConversions[device.price.currency]
          ).toFixed()
        : device.price?.avg;
  }
}

// Detect link icons
function detectLinkIcons(device) {
  let detectLink = (link) => {
    let detectedLink = "rss";
    for (let linkRegex of linkDetection) {
      let pattern = new RegExp(linkRegex.regex);
      if (!!pattern.test(link.link)) {
        detectedLink = linkRegex.icon;
        break;
      }
    }
    link.icon = detectedLink;
  };

  device.externalLinks?.forEach(detectLink);
  device.communityHelp?.forEach(detectLink);
}

// Check maintainership status
function checkMaintained(device) {
  // Find who is maintainer
  device.contributors
    ?.filter((c) => c.role == "Maintainer")
    .forEach((maintainer) => {
      if (!maintainer.renewals?.length) {
        maintainer.role = "Developer";
        return;
      }
      let expiryDate = new Date(maintainer.renewals.at(-1));
      expiryDate.setMonth(expiryDate.getMonth() + 6);
      maintainer.role =
        !maintainer.expired && expiryDate.getTime() > new Date().getTime()
          ? "Maintainer"
          : "Developer";
    });

  device.phoneMakerSupport = !!device.contributors?.filter(
    (c) => c.role == "Phone maker" && !c.expired
  ).length;

  // Remove device.tag part to fully enable maintainership checking when enough data is collected
  device.isMaintained =
    device.phoneMakerSupport ||
    !!device.contributors?.filter((c) => c.role == "Maintainer").length ||
    device.tag != "unmaintained";
}

// Replace ID with name
function useNameInsteadOfFeatureId(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature) {
      graphQlFeature.name = feature.name;
    }
  });
}

// Delete categories that have no features set
function deleteVoidCategories(device) {
  for (let portCategory in portStatus) {
    let category = dataUtils.getCategoryByName(device, portCategory);

    if (category && category.features.length == 0) {
      device.portStatus.splice(device.portStatus.indexOf(category), 1);
    }
  }
}

// Elaborate device info from IDs
function useNameInsteadOfSpecificationId(device) {
  device.deviceInfo.forEach((el) => {
    el.name = deviceInfo.find((info) => info.id == el.id).name;
  });
}

// Remove development commits from the data history
function removeIgnoredCommits(device) {
  for (let commit = device.gitData.length - 2; commit >= 0; commit--) {
    if (ignoredCommits.includes(device.gitData[commit].hash)) {
      device.gitData.splice(commit, 1);
    }
  }
}

// Set imported images
async function setImportedImages(device) {
  if (device.externalLinks) {
    let extLinks = [];

    for (let link of device.externalLinks) {
      extLinks.push({
        name: link.name,
        link: link.link,
        icon: (await import(`../../assets/img/services/${link.icon}.svg`))
          .default
      });
    }
    device.externalLinks = extLinks;
  }

  if (device.communityHelp) {
    let extLinks = [];

    for (let link of device.communityHelp) {
      extLinks.push({
        name: link.name,
        link: link.link,
        icon: (await import(`../../assets/img/services/${link.icon}.svg`))
          .default
      });
    }
    device.communityHelp = extLinks;
  }
}

export default pMemoize(elaborate);
