import { empty } from "empty-schema";
import YAML from "js-yaml";
import { dataRules } from "@data/validationRules.js";
import { deviceInfoAsArray } from "@logic/build/dataUtils.js";

const rules = dataRules();

export async function get() {
  let data = empty(rules);
  data.deviceInfo = deviceInfoAsArray(data.deviceInfo);

  return {
    body:
      "---\n" + YAML.dump(data, { forceQuotes: true, quotingType: '"' }) + "---"
  };
}
